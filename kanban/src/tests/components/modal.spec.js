import { mount } from "@vue/test-utils";
import Modal from "../../components/modal.vue";

describe("modal.vue", () => {
  const factory = () => {
    return mount(Modal, {
      data() {
        return {
          item: {
            title: "Test title",
            description: "Test description",
          },
        };
      },
    });
  };

  test("Display item in modal", () => {
    const wrapper = factory();

    const inputs = wrapper.findAll("input");

    const titleInput = inputs.wrappers[0].element;
    const descriptionInput = inputs.wrappers[1].element;

    expect(titleInput.value).toBe("Test title");
    expect(descriptionInput.value).toBe("Test description");
  });

  test("Click on 'add' triggers add event", async () => {
    const wrapper = factory();
    const addButton = wrapper.find("#add-button");

    await addButton.trigger("click");

    console.log(wrapper.emitted().add);

    expect(wrapper.emitted().add).toBeTruthy();
  });

  test("Click on 'close' triggers close event", async () => {
    const wrapper = factory();
    const addButton = wrapper.find("#close-button");

    await addButton.trigger("click");

    expect(wrapper.emitted().close).toBeTruthy();
  });
});
