import { mount, createLocalVue } from "@vue/test-utils";
import List from "../../components/list.vue";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(fas);

const localVue = createLocalVue();
localVue.component("font-awesome-icon", FontAwesomeIcon);

describe("list.vue", () => {
  const items = [
    {
      title: "Test Item 1",
      description: "Test description 1",
      id: 1,
    },
    {
      title: "Test Item 2",
      description: "Test description 2",
      id: 2,
    },
    {
      title: "Test Item 3",
      description: "Test description 3",
      id: 3,
    },
  ];

  const factory = (propsData) => {
    return mount(List, {
      localVue,
      propsData: {
        items,
        ...propsData,
      },
    });
  };

  test("Display 3 items in list", () => {
    const wrapper = factory();
    const items = wrapper.findAll(".item");

    expect(items.length).toBe(3);
  });

  test("Click on 'add' triggers modal open event", async () => {
    const wrapper = factory();
    const addButton = wrapper.find(".add-button");

    await addButton.trigger("click");

    expect(wrapper.emitted().open).toBeTruthy();
  });

  test("Click on 'delete' triggers delete event", async () => {
    const wrapper = factory();
    const deleteButton = wrapper.find(".delete-button");

    await deleteButton.trigger("click");

    expect(wrapper.emitted().delete).toBeTruthy();
  });

  
});
