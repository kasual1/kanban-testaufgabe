import { mount } from "@vue/test-utils";
import ListItem from "../../components/list-item.vue";

describe("list-item.vue", () => {
  test("Display content correctly", () => {
    const wrapper = mount(ListItem, {
      propsData: {
        item: {
          title: "Test Title",
          description: "Test description",
          id: 1,
        },
      },
    });

    expect(wrapper.text()).toContain("Test Title");
    expect(wrapper.text()).toContain("Test description");
    expect(wrapper.text()).toContain("#1");
  });
});
