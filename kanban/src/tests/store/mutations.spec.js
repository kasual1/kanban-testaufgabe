import * as ItemStore from "../../store/modules/items";

const {
  default: { mutations },
} = ItemStore;

describe("items.js mutations", () => {
  test("Add item to 'Backlog'", () => {
    const state = { backlog: [], currentMaxId: 0 };
    const item = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    mutations.addToBacklog(state, item);

    expect(state.backlog).toEqual([
      { id: 1, title: "Test Title", description: "Test description" },
    ]);
    expect(state.currentMaxId).toBe(1);
  });

  test("Add item to 'inProgress'", () => {
    const state = { progress: [], currentMaxId: 0 };
    const item = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    mutations.addToInProgress(state, item);

    expect(state.progress).toEqual([
      { id: 1, title: "Test Title", description: "Test description" },
    ]);
    expect(state.currentMaxId).toBe(1);
  });

  test("Add item to 'Done'", () => {
    const state = { done: [], currentMaxId: 0 };
    const item = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    mutations.addToDone(state, item);

    expect(state.done).toEqual([
      { id: 1, title: "Test Title", description: "Test description" },
    ]);
    expect(state.currentMaxId).toBe(1);
  });

  test("Update 'Backlog'", () => {
    const state = { backlog: [] };
    const updatedBacklogItems = [
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 2,
        title: "Test Title 2",
        description: "Test description 2",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ];

    mutations.updateBacklog(state, updatedBacklogItems);

    expect(state.backlog).toEqual(updatedBacklogItems);
    expect(state.backlog.length).toBe(3);
  });

  test("Update 'InProgress'", () => {
    const state = { progress: [] };
    const updatedInProgressItems = [
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 2,
        title: "Test Title 2",
        description: "Test description 2",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ];

    mutations.updateInProgress(state, updatedInProgressItems);

    expect(state.progress).toEqual(updatedInProgressItems);
    expect(state.progress.length).toBe(3);
  });

  test("Update 'Done'", () => {
    const state = { done: [] };
    const updatedDoneItems = [
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 2,
        title: "Test Title 2",
        description: "Test description 2",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ];

    mutations.updateDone(state, updatedDoneItems);

    expect(state.done).toEqual(updatedDoneItems);
    expect(state.done.length).toBe(3);
  });

  test("Remove item from 'Backlog'", () => {
    const state = {
      backlog: [
        {
          id: 1,
          title: "Test Title 1",
          description: "Test description 1",
        },
        {
          id: 2,
          title: "Test Title 2",
          description: "Test description 2",
        },
        {
          id: 3,
          title: "Test Title 3",
          description: "Test description 3",
        },
      ],
      currentMaxId: 3,
    };

    const removedItem = {
      id: 2,
      title: "Test Title 2",
      description: "Test description 2",
    };

    mutations.removeFromBacklog(state, removedItem);

    expect(state.backlog).toEqual([
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ]);
    expect(state.currentMaxId).toBe(3);
  });

  test("Remove item from 'InProgress'", () => {
    const state = {
      progress: [
        {
          id: 1,
          title: "Test Title 1",
          description: "Test description 1",
        },
        {
          id: 2,
          title: "Test Title 2",
          description: "Test description 2",
        },
        {
          id: 3,
          title: "Test Title 3",
          description: "Test description 3",
        },
      ],
      currentMaxId: 3,
    };

    const removedItem = {
      id: 2,
      title: "Test Title 2",
      description: "Test description 2",
    };

    mutations.removeFromInProgress(state, removedItem);

    expect(state.progress).toEqual([
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ]);
    expect(state.currentMaxId).toBe(3);
  });

  test("Remove item from 'Done'", () => {
    const state = {
      done: [
        {
          id: 1,
          title: "Test Title 1",
          description: "Test description 1",
        },
        {
          id: 2,
          title: "Test Title 2",
          description: "Test description 2",
        },
        {
          id: 3,
          title: "Test Title 3",
          description: "Test description 3",
        },
      ],
      currentMaxId: 3,
    };

    const removedItem = {
      id: 2,
      title: "Test Title 2",
      description: "Test description 2",
    };

    mutations.removeFromDone(state, removedItem);

    expect(state.done).toEqual([
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ]);
    expect(state.currentMaxId).toBe(3);
  });
});
