import * as ItemStore from "../../store/modules/items";

const {
  default: { getters },
} = ItemStore;

describe("items.js getters", () => {
  test("Get backlog items", () => {
    const state = {
      backlog: [
        {
          id: 1,
          title: "Test Title 1",
          description: "Test description 1",
        },
        {
          id: 2,
          title: "Test Title 2",
          description: "Test description 2",
        },
        {
          id: 3,
          title: "Test Title 3",
          description: "Test description 3",
        },
      ],
    };

    const result = getters.backlogItems(state);

    expect(result).toEqual(state.backlog);
  });

  test("Get inProgress items", () => {
    const state = {
      progress: [
        {
          id: 1,
          title: "Test Title 1",
          description: "Test description 1",
        },
        {
          id: 2,
          title: "Test Title 2",
          description: "Test description 2",
        },
        {
          id: 3,
          title: "Test Title 3",
          description: "Test description 3",
        },
      ],
    };

    const result = getters.inProgressItems(state);

    expect(result).toEqual(state.progress);
  });

  test("Get done items", () => {
    const state = {
      done: [
        {
          id: 1,
          title: "Test Title 1",
          description: "Test description 1",
        },
        {
          id: 2,
          title: "Test Title 2",
          description: "Test description 2",
        },
        {
          id: 3,
          title: "Test Title 3",
          description: "Test description 3",
        },
      ],
    };

    const result = getters.doneItems(state);

    expect(result).toEqual(state.done);
  });
});
