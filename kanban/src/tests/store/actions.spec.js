import * as ItemStore from "../../store/modules/items.js";
import sinon from "sinon";

const {
  default: { actions },
} = ItemStore;

describe("items.js actions", () => {
  test("Add item to 'Backlog'", () => {
    const item = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    const commit = sinon.spy();
    const state = {};

    actions.addToBacklog({ commit, state }, item);

    expect(commit.args).toEqual([
      [
        "addToBacklog",
        { id: 1, title: "Test Title", description: "Test description" },
      ],
    ]);
  });

  test("Add item to 'InProgress'", () => {
    const item = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    const commit = sinon.spy();
    const state = {};

    actions.addToInProgress({ commit, state }, item);

    expect(commit.args).toEqual([
      [
        "addToInProgress",
        { id: 1, title: "Test Title", description: "Test description" },
      ],
    ]);
  });

  test("Add item to 'Done'", () => {
    const item = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    const commit = sinon.spy();
    const state = {};

    actions.addToDone({ commit, state }, item);

    expect(commit.args).toEqual([
      [
        "addToDone",
        { id: 1, title: "Test Title", description: "Test description" },
      ],
    ]);
  });

  test("Update 'Backlog'", () => {
    const updatedBacklogItems = [
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 2,
        title: "Test Title 2",
        description: "Test description 2",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ];
    const commit = sinon.spy();
    const state = {};

    actions.updateBacklog({ commit, state }, updatedBacklogItems);

    expect(commit.args).toEqual([["updateBacklog", updatedBacklogItems]]);
  });

  test("Update 'InProgress'", () => {
    const updatedInProgressItems = [
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 2,
        title: "Test Title 2",
        description: "Test description 2",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ];
    const commit = sinon.spy();
    const state = {};

    actions.updateInProgress({ commit, state }, updatedInProgressItems);

    expect(commit.args).toEqual([["updateInProgress", updatedInProgressItems]]);
  });

  test("Update 'Done'", () => {
    const updatedDoneItems = [
      {
        id: 1,
        title: "Test Title 1",
        description: "Test description 1",
      },
      {
        id: 2,
        title: "Test Title 2",
        description: "Test description 2",
      },
      {
        id: 3,
        title: "Test Title 3",
        description: "Test description 3",
      },
    ];
    const commit = sinon.spy();
    const state = {};

    actions.updateDone({ commit, state }, updatedDoneItems);

    expect(commit.args).toEqual([["updateDone", updatedDoneItems]]);
  });

  test("Remove item from 'Backlog'", () => {
    const removedItem = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    const commit = sinon.spy();
    const state = {};

    actions.removeFromBacklog({ commit, state }, removedItem);

    expect(commit.args).toEqual([["removeFromBacklog", removedItem]]);
  });

  test("Remove item from 'InProgress'", () => {
    const removedItem = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    const commit = sinon.spy();
    const state = {};

    actions.removeFromInProgress({ commit, state }, removedItem);

    expect(commit.args).toEqual([["removeFromInProgress", removedItem]]);
  });

  test("Remove item from 'Done'", () => {
    const removedItem = {
      id: 1,
      title: "Test Title",
      description: "Test description",
    };

    const commit = sinon.spy();
    const state = {};

    actions.removeFromDone({ commit, state }, removedItem);

    expect(commit.args).toEqual([["removeFromDone", removedItem]]);
  });
});
