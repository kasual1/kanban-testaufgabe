const state = {
  backlog: [
    {
      id: 1,
      title: "Edit Kanban Item",
      description:
        "As a user I want to be able to edit an existing Kanban item.",
    },
    {
      id: 2,
      title: "Persist Kanban board on reload",
      description:
        "As a user I want to be able to see the kanban items even after I refresh the page or close my browser.",
    },
  ],

  progress: [
    {
      id: 3,
      title: "Increase test coverage to 100%",
      description: "The total test coverage of the app should be at 100%.",
    },
  ],

  done: [
    {
      id: 4,
      title: "Add Item",
      description:
        "As a user I want to be able to add items to the kanban board",
    },
    {
      id: 5,
      title: "Move Item",
      description:
        "As a user I want to be able to move items across the different lists in the kanban board",
    },
    {
      id: 6,
      title: "Remove Item",
      description:
        "As a user I want to be able to delete items from the kanban board",
    },
  ],

  currentMaxId: 6,
};

const getters = {
  backlogItems: (state) => state.backlog,

  inProgressItems: (state) => state.progress,

  doneItems: (state) => state.done,

  allItems: () => state.backlog.concat(state.progress, state.done),

  itemsCount: () => state.backlog.concat(state.progress, state.done).length,

  currentMaxId: () => state.currentMaxId,
};

const actions = {
  addToBacklog({ commit }, item) {
    commit("addToBacklog", item);
  },

  addToInProgress({ commit }, item) {
    commit("addToInProgress", item);
  },

  addToDone({ commit }, item) {
    commit("addToDone", item);
  },

  updateBacklog({ commit }, items) {
    commit("updateBacklog", items);
  },

  updateInProgress({ commit }, items) {
    commit("updateInProgress", items);
  },

  updateDone({ commit }, items) {
    commit("updateDone", items);
  },

  removeFromBacklog({ commit }, item) {
    commit("removeFromBacklog", item);
  },

  removeFromInProgress({ commit }, item) {
    commit("removeFromInProgress", item);
  },

  removeFromDone({ commit }, item) {
    commit("removeFromDone", item);
  },
};

const mutations = {
  addToBacklog(state, item) {
    state.currentMaxId++;
    state.backlog.push(item);
  },

  addToInProgress(state, item) {
    state.currentMaxId++;
    state.progress.push(item);
  },

  addToDone(state, item) {
    state.currentMaxId++;
    state.done.push(item);
  },

  updateBacklog(state, items) {
    state.backlog = items;
  },

  updateInProgress(state, items) {
    state.progress = items;
  },

  updateDone(state, items) {
    state.done = items;
  },

  removeFromBacklog(state, item) {
    state.backlog = state.backlog.filter((i) => i.id !== item.id);
  },

  removeFromInProgress(state, item) {
    state.progress = state.progress.filter((i) => i.id !== item.id);
  },

  removeFromDone(state, item) {
    state.done = state.done.filter((i) => i.id !== item.id);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
