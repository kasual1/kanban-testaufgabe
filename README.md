# Aufgabenstellung
Es soll ein einfaches Kanban-Board erstellt werden. Das Kanban-Board soll in der Lage sein neue Aufgaben zu erstellen und zu verwalten.

## Kanban-Board
Das Kanban-Board soll über drei Spalten verfügen, die den aktuellen Status einer Aufgabe wiederspiegelt.

* Backlog (Grau)
* In Progress (Orange)
* Done (Grün)

Eine Spalte kann eine oder mehrere Aufgaben beinhalten. Die Spalten sollen farblich dem Status entsprechen.

Eine Aufgabe soll aus einem Titel und einer Kurzbeschreibung bestehen.

Der Status kann beliebig geändert werden:
* Backlog => Done
* Backlog => In Progress
* Done => In Progress
* Done => Backlog
* In Progress => Backlog
* In Progress => Done

## Technische Anforderungen
Technisch gibt es nur einige wenige Anforderungen:

* Vue.js (latest)
* Die Entwicklung soll in einem eigenen Git-Branch erfolgen
* Das Board soll Responsive aufgebaut sein
* Die Componenten sollen mit Tests (JEST) abgedeckt werden.

# Zeitlicher Rahmen
Die Aufgabe sollte nach Möglichkeit nicht länger als 1 Woche in Anspruch nehmen. Um den Fortschritt des aktuellen Standes zu sehen und ggf. Hilfestellungen zu geben, wäre es von Vorteil zwischendurch in das Repository zu pushen.
